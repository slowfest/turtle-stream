#!/usr/bin/env bash

environment=production.persistent

if [ ! -d ${environment} ]
then
  echo INSTALL [${environment}]
  python3 -m venv ${environment}
  source ${environment}/bin/activate
  pip install --upgrade pip setuptools
  pip install --requirement production-requirements.txt
else
  echo SKIPPED [${environment} is already installed]
fi
