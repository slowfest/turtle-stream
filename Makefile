lint: continuous-integration
	./production.persistent/bin/ansible-lint -x701 playbook.yml

galaxy: continuous-integration
	./production.persistent/bin/ansible-galaxy install --role-file requirements.yml --force --roles-path common_roles

continuous-integration:
	./install_ansible_tools.sh

mrproper:
	rm -r production.persistent

deploy: galaxy lint
	./production.persistent/bin/ansible-playbook -i inventory $(ANSIBLE_PARAMS) playbook.yml --diff

# -------- #
# System   #
# -------- #

users: # Create users
	./production.persistent/bin/ansible-playbook -i inventory $(ANSIBLE_PARAMS) playbook-users.yml --diff

users-as-pi: # Create users with pi user (to deploy first user)
	./production.persistent/bin/ansible-playbook -i inventory $(ANSIBLE_PARAMS) playbook-users.yml -e "ansible_user=pi" --diff --ask-pass
