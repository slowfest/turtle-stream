... This project is under construction ...

# Turtle Stream

Use this ansible role to deploy the Turtle Stream. Two Raspberry pis are needed:
one for the webserver, the other for the cam.

## Install the project

```bash
git clone https://framagit.org/slowfest/turtle-stream
cd turtle-stream
```

### Install python virtualenv

```bash
make continuous-integration
```

### Install role dependencies

```bash
make galaxy
```

## Configuration

### Create the inventory

Give to Ansible the Raspberry Pis reference. It will deploy the app on it.

```bash
# edit the inventory file into the project root
[all]
10.10.10.10 # IP or domain for the first pi
10.10.10.11 # IP or domain for the second pi
```

### Add users to the pi

```bash
# edit the playbook-user.yml file
- hosts: all
  become: yes
  roles:
    - role: unix_user
      user:
        login: <your login>
        uid: <your UID>>
        group:
          name: <your group name>
          id: <your GID>
        comment: <give a comment on the user>
        shell: /bin/bash # or an other shell
        authorized_keys: <path to your public key>
```
